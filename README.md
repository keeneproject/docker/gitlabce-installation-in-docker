# GitlabCE installation via Docker-compose

NOTE: This information is rather old and maintaining your own GitLab instance is rather complex.  I would recommend using Kubernetes and installing via the [GitLab Helm Chart](https://docs.gitlab.com/charts/installation/).

How to setup and maintain a GitLab CE instance via docker with docker-compose including gitlab runners.  I have also included some instructions on using minikube and how to spin up gitlab for testing in kubernetes.

## gitlabce requirements
1. Requirements outlined [here](https://docs.gitlab.com/ee/install/requirements.html)
2. GitLab recommends 4 cpu and 4 GB ram per 500 users
3. For testing in a vm I have limited gitlabce to 3GB ram and 2 cpu and the gitlab-runner to 2GB ram and 1 cpu.
4. Be aware that systemd-oomd may aggressively kill your apps if you dont have enough ram. you may need to increase ram, swap, or disable systemd-oomd
5. Adjust limits as needed, follow the gitlab recommendations if you have enough cpu and ram.


## Install docker and docker-compose
1. Install docker via instructions [here](https://docs.docker.com/engine/install/)
2. Install docker-compose via instructions [here](https://docs.docker.com/compose/install/)
3. Test docker by running a docker image
```
docker run --rm hello-world
```
## Start gitlabce
1. Clone this project and docker-compose up -d  
  
**NOTE:** This docker-compose will start gitlab-ce with no certificates on http only.  If you want to enable https, install your own certs as outlined below or set letsencrypt to true and change the urls to https in the docker-compose.yml if you want to use letsencrypt.  
**NOTE:** This compose file is using docker managed persistant volumes with will end up somewhere in /var/lib/docker/volumes/  you could also use host mounted volumes.

```
git clone https://gitlab.com/keeneproject/docker/gitlabce-installation-in-docker.git
cd gitlabce-installation-in-docker
docker-compose up -d
# Give it time to come up and check logs.
docker-compose logs -f

# Check mem usage
docker stats
```
2. Wait for gitlab to spin up and login to your gitlab instance at http://localhost
3. Get the first time root login password as outlined [here](https://docs.gitlab.com/omnibus/installation/)
```
docker exec -it gitlabce cat /etc/gitlab/initial_root_password
```

## Register your gitlab runners
1. GitLab doco on registering a runner located [here](https://docs.gitlab.com/runner/register/)
2. Go to admin -> runners -> copy registration token
3. Run the following command to register the runner. update --registration-token first
   You can run this multiple times to create many docker executers on that server.  
```
docker exec -it gitlab-runner \
  gitlab-runner register \
    --non-interactive \
    --registration-token yxTooP9FyvAywLrKC2m2 \
    --locked=false \
    --description docker-runner-3 \
	--tag-list docker  \
    --run-untagged="true" \
    --url http://gitlabce  \
	--clone-url http://gitlabce \
	--executor docker \
    --docker-image docker:stable \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-network-mode gitlabce-installation-in-docker_default

##NOTES:
1. Tags can be used to tell jobs to run on certain runners.
2. Update the executor if you install a runner somewhere else. shell, kubernetes, etc
3. -url needs to be a way for the runner to reach the gitlabce server. In this case we are using the container name and let docker manage the 4. networking.
5. - clone-url is so containers you spin up can clone your code
6. --docker-network-mode is required to tell new containers you spin up what network to be on so it can reach the gitlab server
   IE: You run IMAGE: ubuntu:latest in a gitlabci stage
7. If you put a gitlab runner in docker on a different host. Remove the networkmode as the urls will have to be able to be resolved normally.
```
4. Test your runner
```
1. Create new project
2. New file -> name it .gitlab-ci.yml
3. Enter the following contents

build-stage:
  image: ubuntu:latest
  stage: build
  script:
    - echo "starting build stage"
    - echo "uname -a"
    - cat /etc/issue
    - apt update

4. Commit and refresh the page
5. You should see the pipeline start and finish
6. If it doesnt check the logs. You can also use the left menu to go to CI/CD -> pipelines
```

## delete everything
```
# Stop containers and networks etc
docker compose down

# If you stopped the containers volume prune will delete all volumes not being used by a container.
# Careful you dont blow away your other volumes you use. you can rm them manually if you prefer
docker volume prune

```
## OPTIONAL - install your own certificates for both gitlab and gitlab runners
If you want to install your own certificates follow this step. You can create the volumes and add the certs before you initially run docker-compose up -d, or you can start it up on http then add the certs while it is running then modify your compose and docker-compose restart.

1. Request a ssl certificate for your host or generate your own.
2. You will probably have to extract the private key and public key from the .pfx into .crt and .key format. instructions on gitlab documentation or google.
3. Copy the host certificates into the gitlab CE /etc/gitlab/ssl/ directory. both the public .crt and private .key files
4. Copy the gitlab public .crt file into the gitlab runner /etc/gitlab-runner/certs/ directory
5. Additionally you may need to install your CA bundles from /etc/pki/tls/certs/*.crt into /etc/gitlab/ssl/ and /etc/gitlab/trusted-certs/ on the gitlab CE server
6. Additionally you may need to install your CA bundles from /etc/pki/tls/certs/*.crt into /etc/gitlab-runner/certs on the gitlab CE server

##  setup backups for gitlab
Run daily backup via cron and rsync backup to another server.
take backup, rsync to 2nd host, keep last 30 backups only.
```
# On gitlab host
$ sudo crontab -e
# Take a backup in the default location which is the docker volume location of /var/opt/gitlabbackups/
0 1 * * * docker exec -t gitlabce gitlab-backup create
# rsync it to another server
0 2 * * * rsync -r 'dockervolume location of /var/opt/gitlabbackups/' username@server2:/pathtobackups/
0 0 * * 0 ls -1t 'dockervolume location of /var/opt/gitlabbackups/' | tail -n +30 | xargs rm

# On 2nd server to store offsite backups add cleanup job.
$ sudo crontab -e
0 0 * * 0 ls -1t pathtobackups | tail -n +30 | xargs rm
```

##  How to restore for backups / disaster recovery
The following steps outline spinning up a new instance and restoring into it.
```
# The version you are restoring to of gitlab must be the same.  Pull that version

$ vi docker-compose.yml #this is a minimal docker-compose with no persistant storage just to test restore.
version: "2.2"
services:
  mygitlabcetest:
    # The version you are restoring to of gitlab must be the same.  Pull that version
    image: 'gitlab/gitlab-ce:13.6.3-ce.0'
    hostname: 'gitlabcetest'
    container_name: gitlabce
    restart: always
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://localhost'
        letsencrypt['enable'] = false
        nginx['redirect_http_to_https'] = false
        gitlab_rails['gitlab_shell_ssh_port'] = 2222

    ports:
      - '80:80'
      - '2222:22'
      - '5050:5050'
    shm_size: '256m'
    deploy:
      resources:
          limits:
            #limit to 2 cpus
            cpus: 2.0
            memory: 3072M
$ docker-compose up -d
$ docker cp 1610031645_2021_01_07_13.6.3_gitlab_backup.tar gitlabce:/var/opt/gitlab/backups/
$ docker exec -it gitlabce bash
$ chown git:git /var/opt/gitlab/backups/1610031645_2021_01_07_13.6.3_gitlab_backup.tar
$ ll /var/opt/gitlab/backups

# Stop the processes that are connected to the database
$ docker exec -it gitlabce gitlab-ctl stop unicorn
$ docker exec -it gitlabce gitlab-ctl stop puma
$ docker exec -it gitlabce gitlab-ctl stop sidekiq

# Verify that the processes are all down before continuing
$ docker exec -it gitlabce gitlab-ctl status

# Run the restore
$ docker exec -it gitlabce gitlab-backup restore BACKUP=1610031645_2021_01_07_13.6.3
# Restart the GitLab container
$ docker-compose restart

#login to gitlab to see if restore was a success.
```


## Tip: How to move default docker directory
In case you need to move your docker directory afterwards
```
$ sudo service docker stop
$ sudo mkdir /opt/newlocation/docker
$ sudo chown root:root /opt/newlocation/docker
$ sudo chmod 755 /opt/newlocation/docker
$ sudo vi /etc/docker/daemon.json #add "graph": "/opt/newlocation/docker/"    ---- once deprecated use "data-root": "/opt/newlocation/docker" 
$ sudo rsync -aP /var/lib/docker/ /opt/newlocation/docker
$ sudo mv /var/lib/docker /var/lib/docker.old
$ sudo service docker start
$ sudo reboot
```


## Configure docker http proxy (optional step on how you would configure docker with a proxy)
```
sudo mkdir /etc/systemd/system/docker.service.d/
sudo vi /etc/systemd/system/docker.service.d/http-proxy.conf
#add
[Service]
Environment="HTTP_PROXY=http://myproxy.com:port/"
Environment="HTTPS_PROXY=http://myproxy.com:port/"
sudo systemctl daemon-reload
sudo systemctl restart docker
docker run --rm hello-world

```

## Running gitlabce-deloyment.yaml in kubernetes
**NOTE:** this is just for testing so it may not the optimal way to run gitlab in kubernetes. you are probably better off using the gitlab helm charts.
```
# Apply deployment
kubectl apply -f gitlabce-deloyment.yaml

# Check things while we wait for it to come up
kubectl get all -o wide

# Browse to you app via Ingress

kubectl get ingress
sudo vi /etc/hosts #and add the ingress ip and hostname
192.168.49.2    mygitlab.local

Then browse to 
http://mygitlab.local

# In worst case browse to your app manually.
minikube service mygitlabce-service --url #to get urls
Access via http://NODEIPfromgetnodes:HIGHportfromgetpodswide 

# Once up 
kubectl exec -it mygitlabce-deployment-644d78b5-452j9 -- cat /etc/gitlab/initial_root_password

# Configure runner 
exec into the runner pod
gitlab-runner register
instance URL = http://mygitlab.local
NOTE: You could use http://mygitlabce-service/ if you didn't edit /etc/hosts. However  git clone wont work in jobs unless you modify the external hostname.
enter token
select kubernetes executer
```

## How to convert docker-compose.yml to kubernetes deployment file
1. Install kompose and convert
```
sudo dnf install kompose
compose convert
kubectl apply -f <output file>
#it way mess up the labels
```

## How to install and use minikube for local kubernetes cluster
1. Install [minikube](https://minikube.sigs.k8s.io/docs/start/)
```
#on fedora
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -Uvh minikube-latest.x86_64.rpm

#on ubuntu
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

2. Start your cluster
```
minikube start

#you can increase resources used from the default of 2gb ram 2 cpu. Watch the minikube startup for errors if you overallcate too much memmory you may have issues
minikube start --memory 4096 --cpus 3
minikube dashboard

#isntall addons
minikube addons list
minikube addons enable dashboard
```

3. Interact with your cluster
```
minikube kubectl -- get po -A

#or alias it
alias kubectl="minikube kubectl --"
```

4. Deploy applications
```
kubectl apply -f deployment.yaml
```

5. Manage your cluster
```
#Pause Kubernetes without impacting deployed applications:
minikube pause
minikube unpause

#Halt the cluster:
minikube stop

#Increase the default memory limit (requires a restart):
minikube config set memory 16384

#Browse the catalog of easily installed Kubernetes services:
minikube addons list

#Create a second cluster running an older Kubernetes release:
minikube start -p aged --kubernetes-version=v1.16.1

#Delete all of the minikube clusters:
minikube delete --all

#get url
minikube service mygitlabce-service --url #will show the ip and port to get directly to the pods
```

6. Other kubectl commands
```
kubectl get all --all-namespaces
kubectl get deployments
kubectl get pods -o wide
kubectl logs -f podname
kubectl describe pods
kubectl get nodes
kubectl cluster-info
kubectl exec -it podname -- bash
```
## How to install and use [kind kubernetes](https://kind.sigs.k8s.io/)
Runs a single node kubernetes cluster inside a single docker container. Pretty close to bare bones cluster via kubeadm
```
#prereq
docker

# Install
sudo curl -Lo /usr/bin/kind https://kind.sigs.k8s.io/dl/v0.12.0/kind-linux-amd64
chmod +x /usr/bin/kind

# Create cluster
kind create cluster  #can create multiple
kubectl cluster-info
kubectl get all --all-namespaces

# Deploy app
kubectl apply -f deployment.yaml
kubectl get all

# Optional - deploy addons such as dashboard
-Dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
dashboard at http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/
kubectl create serviceaccount dashboard-admin-sa
kubectl create clusterrolebinding dashboard-admin-sa --clusterrole=cluster-admin --serviceaccount=default:dashboard-admin-sa
kubectl get secrets
kubectl describe secret dashboard-admin-sa-token-45ljv
#copy token for dashboard

# Delete cluster
kind delete cluster
```

## Links
**Installing docker**  
https://docs.docker.com/engine/install/  
https://docs.docker.com/compose/install/  

**Installing gitlab in docker**  
https://docs.gitlab.com/ee/install/docker.html  
https://docs.gitlab.com/omnibus/installation/
https://docs.gitlab.com/ee/install/requirements.html

**Installing gitlab runners and executors**  
https://docs.gitlab.com/runner/register/  
https://docs.gitlab.com/runner/executors/  

**setting up ingress**
https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/


